package hlf

import (
	"encoding/json"
	"fmt"
	"gitlab.com/hlf-mipt/basic-asset-core/pkg/model"
)

func (svc *HLFService) CreateAsset(user string, item *model.Asset) error {
	contract, err := svc.getContract(user)
	if err != nil {
		return err
	}

	buf, err := json.Marshal(*item)
	if err != nil {
		return fmt.Errorf("failed to json marshalling: %v", err)
	}

	_, err = contract.SubmitTransaction("CreateAsset", string(buf))
	if err != nil {
		return fmt.Errorf("failed to Submit transaction: %v", err)
	}

	return nil
}
