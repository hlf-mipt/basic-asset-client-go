package logger

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal/config"
)

func NewLogger(cfg *config.Config) *logrus.Entry {
	level, _ := logrus.ParseLevel(cfg.LogLevel)
	logrus.SetLevel(level)

	return logrus.WithFields(logrus.Fields{
		"app_name": cfg.AppName,
		"version":  cfg.AppVersion,
	})
}
