package httphandler

import (
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal/service/hlf"
	"gitlab.com/hlf-mipt/basic-asset-core/pkg/model"
	"net/http"
)

type HttpHandler struct {
	log *logrus.Entry
	hlf *hlf.HLFService
}

func NewHttpHandler(log *logrus.Entry, hlf *hlf.HLFService) *HttpHandler {
	return &HttpHandler{
		log: log,
		hlf: hlf,
	}
}
func (ctrl *HttpHandler) CreateAsset(e echo.Context) error {
	user := e.Request().Header.Get("User")
	req := new(model.Asset)
	err := e.Bind(req)
	if err != nil {
		ctrl.log.Errorf("failed to bind request: %v", err)
		return echo.NewHTTPError(http.StatusBadRequest, "invalid input data")
	}

	err = ctrl.hlf.CreateAsset(user, req)
	if err != nil {
		ctrl.log.Errorf("failed to create asset: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return e.JSON(http.StatusOK, req)
}
