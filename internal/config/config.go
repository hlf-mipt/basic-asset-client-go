package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"log"
)

type (
	Config struct {
		App
		HTTP
		HLF
	}

	App struct {
		AppName    string `env:"HLFMIPT_APP_NAME" env-default:"basic asset client go"`
		AppVersion string `env:"HLFMIPT_APP_VERSION" env-default:"0.1"`
		LogLevel   string `env:"HLFMIPT_LOG_LEVEL" env-default:"info"`
	}

	HTTP struct {
		BindAddress string `env:"HLFMIPT_BIND_ADDRESS" env-default:"0.0.0.0"`
		BindPort    uint   `env:"HLFMIPT_BIND_PORT" env-default:"8080"`
	}

	HLF struct {
		WalletPath    string `env:"HLFMIPT_WALLET_PATH" env-default:"wallet"`
		ChannelName   string `env:"HLFMIPT_CHANNEL_NAME" env-default:"mychannel"`
		ChaincodeName string `env:"HLFMIPT_CHAINCODE_NAME" env-default:"basic"`
	}
)

func NewWebApiConfig() *Config {
	cfg := &Config{}

	err := cleanenv.ReadEnv(cfg)
	if err != nil {
		log.Fatalf("failed to read config: %v", err)
	}

	return cfg
}
