package main

import (
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal"
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal/config"
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal/httphandler"
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal/logger"
	"gitlab.com/hlf-mipt/asset-transfer-basic/internal/service/hlf"
	"go.uber.org/fx"
)

func main() {

	fx.New(
		fx.Provide(logger.NewLogger),
		fx.Provide(httphandler.NewHttpHandler),
		fx.Provide(config.NewWebApiConfig),
		fx.Provide(hlf.NewHLFService),
		fx.Provide(internal.NewAppService),

		fx.Invoke(func(*hlf.HLFService) {}),
		fx.Invoke(func(*internal.AppService) {}),
	).Run()

}
